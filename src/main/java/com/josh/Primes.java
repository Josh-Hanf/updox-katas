package com.josh;

import java.util.ArrayList;
import java.util.List;

public class Primes implements PrimeNumberGenerator {

	@Override
	public List<Integer> generate(int startingValue, int endingValue) {
		int start;
		int end;
		if(startingValue>endingValue){
			
			start = endingValue;
			end = startingValue;
		}else{
			start = startingValue;
			end = endingValue;
		}
		
		if(start<=0){
			start=1;
		}
		
		List<Integer> primesList = new ArrayList<>();
		
		for (int i = start; i<=end;i++){
			if (isPrime(i)){
				primesList.add(i);
			}
		}

		
		return primesList;
	}

	@Override
	public boolean isPrime(int value) {
		//a number wont be divisible by a number larger than half its value. 
		//just running one by one through the half value!
		int halfValue = value/2;
		int divisibleCounter = 1;
		for(int i =2; i<=halfValue; i++){
			if(value%i==0){
				divisibleCounter++; 
			}
		}
		
		if(divisibleCounter>1){
			return false;
		}else{
			return true;
		}	
	}

	
	
}
