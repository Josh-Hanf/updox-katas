package com.josh;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class main {
	public static void main(String[] args){
		List<Integer> primesList = new ArrayList<>();
		Primes primes = new Primes();
		Scanner scan = new Scanner(System.in);
		int firstInt = 0;
		int secondInt = 0;
		String checker = "";
		boolean firstInputInvalid=true;
		boolean secondInputInvalid =true;
		boolean keepGoing = true;
		
		System.out.println("Hello Updox");
		
		while(keepGoing){
		
			//first input verification
			while(firstInputInvalid){
			System.out.println("We are going to check for primes within a range you provide. I need two integers. Please enter your first Integer");
			checker = scan.nextLine();
				if(checker.matches("[0-9]+")){
					firstInputInvalid = false;
					firstInt = Integer.parseInt(checker);
				}else{
				System.out.println("Please enter a valid integer");
				}
			}
			
			//second input verification
			while(secondInputInvalid){
				System.out.println("Please enter your second number");
				checker = scan.nextLine();
				if(checker.matches("[0-9]+")){
					secondInputInvalid = false;
					secondInt = Integer.parseInt(checker);
				}else{
				System.out.println("Please enter a valid integer");
				}
			}
			
			//output
			primesList = primes.generate(firstInt, secondInt);
			System.out.println("the primes within your parameters are:");
			for(Integer prime:primesList){
				System.out.print(prime + " ");
			}
			System.out.println();
			
			//keep running
			System.out.println("well that was fun! would you like to try another range? (Y) to run again!");
			checker = scan.nextLine();
			if (checker.equals("y") || checker.equals("Y")){
				firstInputInvalid=true;
				secondInputInvalid =true;
				checker="";
				
			}else{
				keepGoing = false;
			}
		
			
		}
		
		System.out.println("thanks for the for your time and the opportunity to interview with Updox!");
		
	}

}
