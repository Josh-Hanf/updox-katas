package com.josh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class _primesTest {
		Primes primes;
		List<Integer> output;
		
	@Before
	public void initialize(){
		this.primes = new Primes();
		this.output = new ArrayList<>();
		
	}
	
	@Test
	public void return_one_is_a_prime(){
		
		output.add(1);
		Assert.assertEquals(output, primes.generate(1, 1));	
	}
	
	@Test
	public void check_numbers_one_through_five(){
		Integer[] ints = new Integer[] {1, 2, 3, 5};
		output.addAll(Arrays.asList(ints));
		
		Assert.assertEquals(output, primes.generate(1, 5));	
	}
	
	@Test
	public void one_through_five_with_compute_with_lower_number_entered_second(){
		Integer[] ints = new Integer[] {1, 2, 3, 5};
		output.addAll(Arrays.asList(ints));
		
		Assert.assertEquals(output, primes.generate(5, 1));	
	}
	
	@Test
	public void negatives_cant_be_primes(){
		Integer[] ints = new Integer[] {1, 2, 3, 5};
		output.addAll(Arrays.asList(ints));
		
		Assert.assertEquals(output, primes.generate(5, -1000));	
	}
	
	@Test
	public void try_lets_the_test_exmple_from_the_updox_pdf(){
		Integer[] ints = new Integer[] {1, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101};
		output.addAll(Arrays.asList(ints));
		
		Assert.assertEquals(output, primes.generate(102, -1000));	
	}
	
	@Test
	public void zero_should_not_show_up(){
		Integer[] ints = new Integer[] {1, 2, 3, 5};
		output.addAll(Arrays.asList(ints));
		
		Assert.assertEquals(output, primes.generate(0, 5));	
	}
	
}
