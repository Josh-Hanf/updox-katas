<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>

<head>
<meta name="viewport" content="width=device-width" />


<c:url var="bootstrapHref" value="/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="${bootstrapHref}">

<c:url var="cssHref" value="/css/style.css" />
<link rel="stylesheet" type="text/css" href="${cssHref}">


<script type="text/javascript" src="js/jquery-3.1.0.js">
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">

<script> src="updox.js"</script>

</head>

<body >

<script type="text/javascript">
 
	$(document).ready(function(){
				
			var initializeOne = 
				[
			    {"last_name": "Harris", "first_name": "Mike", "email_address": "mharris@updox.com", "specialty": "Pediatrics", "practice_name": "Harris Pediatrics"},
			    {"last_name": "Wijoyo", "first_name": "Bimo", "email_address": "bwijoyo@updox.com", "specialty": "Podiatry", "practice_name": "Wijoyo Podiatry"},
			    {"last_name": "Rose", "first_name": "Nate", "email_address": "nrose@updox.com", "specialty": "Surgery", "practice_name": "Rose Cutters"},
			    {"last_name": "Carlson", "first_name": "Mike", "email_address": "mcarlson@updox.com", "specialty": "Orthopedics", "practice_name": "Carlson Orthopedics"},
			    {"last_name": "Witting", "first_name": "Mike", "email_address": "mwitting@updox.com", "specialty": "Pediatrics", "practice_name": "Witting’s Well Kids Pediatrics"},
			    {"last_name": "Juday", "first_name": "Tobin", "email_address": "tjuday@updox.com", "specialty": "General Medicine", "practice_name": "Juday Family Practice"}
			    ];
			
			doTheThing();
			
			
			/* Main sorting function */
			function predicateBy(prop){
				   return function(a,b){
					      if( a[prop] > b[prop]){
					          return 1;
					      }else if( a[prop] < b[prop] ){
					          return -1;
					      }
					      return 0;
					}
			}
	
			
			/* takin care of deletions via check box, actuatated at by the remove button */
			function deleteMarkedCheckedBoxes(){
				var queue = [];
			
				
				for(i=initializeOne.length-1; i>=0 ; i--){
					if($('#' + i).is(":checked")){
						queue.push(i);	
					}
				}
				while(queue.length>0){
					initializeOne.splice(queue.shift(), 1);
				}
				
				updateList();		
			}
			
			function updateList() {
				$( "#top" ).remove();
			   	var topDiv = $('<div>').attr('id', 'top');
			   	$('#head').append(topDiv);
		   	
			   	doTheThing();
			}
			
			/* this is the refresh, where i drop the urrent load and reappend a div full of the new information */
			function doTheThing(){
			
				var counter= 0;
				
				for(k in initializeOne){
					var provider = initializeOne[k];
					
					var mainDiv = $('<div>').addClass('container-fluid').attr('id', 'provider'+counter);
					var checkDiv = $('<div>').addClass('container-fluid col-xs-2 floatLeft');
					var checkBox = $('<input>').attr('type', 'checkbox').attr('name', 'removeProvider'+counter).attr('value', 'removeProvider').attr('id', counter);
					var innerDiv = $('<div>').addClass('container-fluid col-xs-10 floatRight').attr('id', 'innerDiv'+counter);
					
					var firstDiv = $('<div>').addClass('container-fluid').attr('id', 'firstDiv'+ counter);
					var secondDiv = $('<div>').addClass('container-fluid').attr('id', 'secondDiv'+ counter);
					
					var namesP = $('<p>').addClass('floatLeft').attr('id', 'names'+counter).html(provider.last_name + ", " + provider.first_name);
					var specialtyP = $('<p>').addClass('floatRight').attr('id', 'specialty'+counter).html(provider.specialty);
					var emailP = $('<p>').addClass('floatLeft').attr('id', 'email'+counter).html(provider.email_address);
					var practiceP = $('<p>').addClass('floatRight').attr('id', 'practice'+counter).html(provider.practice_name);
					
					checkDiv.append(checkBox);
					mainDiv.append(checkDiv);
					
					firstDiv.append(namesP);
					firstDiv.append(specialtyP);
					
					innerDiv.append(firstDiv);
					
					secondDiv.append(emailP);
					secondDiv.append(practiceP);
					
					innerDiv.append(secondDiv);
					
					mainDiv.append(innerDiv);
					
					$('#top').append(mainDiv);
					
					counter++; 
				}
				
			}
			
				$( "#remove" ).click(function() {
					deleteMarkedCheckedBoxes();
				});
				
				 $("#sortPulldown").change(function(){
					var selected = $('#sortPulldown').find(":selected").text();
					if (selected === "Specialty"){
						initializeOne.sort(predicateBy("specialty"));
						updateList();
					}else if(selected === "First Name"){
						initializeOne.sort(predicateBy("first_name"));
						updateList();
					}else if(selected === "Last Name"){
						initializeOne.sort(predicateBy("last_name"));
						updateList();
					}else if(selected === "Email Address"){
						initializeOne.sort(predicateBy("email_address"));
						updateList();
					}else{
						initializeOne.sort(predicateBy("practice_name"));
						updateList();
					}
				});
			
				$( "#submit" ).click(function() {
					var lastName = $('#lastName').val().trim();
					var firstName = $('#firstName').val().trim();
					var email = $('#email').val().trim();
					var specialty = $('#specialty').val().trim();
					var practice = $('#practice').val().trim();
		
					var provider= {
							  "last_name": lastName,
							  "first_name": firstName,
							  "email_address": email,
							  "specialty": specialty,
							  "practice_name": practice,
							  "balance": 120.50
								}; 
				
				    $( "#top" ).remove();
				   	var topDiv = $('<div>').attr('id', 'top');
				   	$('#head').append(topDiv);
				   	initializeOne.push(provider);
				   	
				   	doTheThing();
				   						
					$('#lastName').val("");
					$('#firstName').val("");
					$('#email').val("");
					$('#specialty').val("");
					$('#practice').val("");
				});		
	});
 
</script>


<div class = "container-fluid border alginMid">
	
	<div>
	<h2>Provider Directory
	</h2>
	</div>
		<div class = "container col-xs-0 col-sm-1"></div>
			<div class = "container col-xs-12 col-sm-3 border">
				<h4>Create Provider</h4><br/>
				<div>
					<label for="lastName">Last Name: </label>
					<input type="text" id="lastName" name="lastName" placeHolder="Last Name" class="form-control" />
				</div>
				<div>
					<label for="firstName">First Name: </label>
					<input type="text" id="firstName" name="firstName" placeHolder="First Name" class="form-control" />
				</div>
				<div>
					<label for="email">Email Address: </label>
					<input type="text" id="email" name="email" placeHolder="Email" class="form-control" />
				</div>
				<div>
					<label for="specialty">Specialty: </label>
					<input type="text" id="specialty" name="specialty" placeHolder="Specialty" class="form-control" />
				</div>
				<div>
					<label for="practice">Practice Name: </label>
					<input type="text" id="practice" name="practice" placeHolder="Practice" class="form-control" />
				</div>
				<div class = "">
					<button id="submit" type="submit">Submit Provider</button>
				</div>
			</div>
		<div class = "container col-xs-12 col-sm-7 border">
		<div id = "head">
		<h4>Provider List</h4>
				<div>
					<div class = "col-xs-6 twentyPixelBottomMargins">
						Sort By:
					</div>
					<div class = "col-xs-6 floatRight twentyPixelBottomMargins ">
						<select name="category" id = "sortPulldown" class = "floatRight">
						    <option value="1" ${provider.specialty == '1' ? 'selected' : ''}>Specialty</option>
						    <option value="2" ${provider.lastName == '2' ? 'selected' : ''}>First Name</option>
						    <option value="3" ${provider.firstName == '3' ? 'selected' : ''}>Last Name</option>
						    <option value="4" ${provider.email == '4' ? 'selected' : ''}>Email</option>
						    <option value="5" ${provider.practice == '5' ? 'selected' : ''}>Practice</option>
						</select>
					</div>
				</div>
			<div id ="top">
				
			</div>
			
		</div>
			<button id="remove" type="submit" class = "floatRight">Remove Selected Providers</button>
			
			<div class = "container-fluid " id = "provider" >
				
			</div>
		</div>
	
</div>




</body>
</html>



